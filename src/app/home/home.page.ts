import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  weight: number;
  gender: string;
  hours: number[] = [];
  hoursC: number;
  bottles: number[] = [];
  bottlesC: number;
  promilles: number;

  constructor() {
  }

  ngOnInit(){
    let i: number;
    for (i = 1; i < 25; i++){
      this.hours.push(i);
      this.bottles.push(i);
    }
  }

  calculate() {
    const litres = this.bottlesC * 0.33;
    const grams = litres * 8 * 4.5;
    const burning = this.weight / 10;
    const gramsleft = grams - (burning * this.hoursC);
    
    
    if (this.gender === 'Male') {
      this.promilles = gramsleft / (this.weight * 0.7);
    } else {
      this.promilles = gramsleft / (this.weight * 0.6);
    }
  }

}